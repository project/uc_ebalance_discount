
Ubercart E-balance discount MODULE FOR DRUPAL 7.x
-------------------------------------------------------

CONTENTS OF THIS README
-----------------------

   * Description
   * Requirements
   * Installation
   * Maintainers

DESCRIPTION:
------------

This module integrates with the Ubercart module and creates a new kind of discount. This discount type works on balance points provided by balance_tracker module.

Once admin credit points regarding users, then that user can purchase products basis on those credit points using E-balance payment method. 
But when user have credit point less than order total then user e-balnce will perform as a discount line item.


REQUIREMENTS:
------------

Requires the following modules:

  - Ubercart (https://www.drupal.org/project/ubercart)
  - Balance Tracker (https://www.drupal.org/project/balance_tracker)


INSTALLATION:
-------------
Install as you would normally install a contributed Drupal module.
See:
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.


MAINTAINERS
--------------------------------------------------------------------
 Current maintainer:
 * Vikas kumar - https://www.drupal.org/u/babusaheb.vikas
